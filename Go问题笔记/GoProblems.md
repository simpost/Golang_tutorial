# Go问题笔记

## 一、时间的获取

使用```time```模块不同的方法获取时间，时间是错误的。如下面代码：

```Golang
package main

import (
	"fmt"
	"time"
)

var week time.Duration

func main() {
	t := time.Now()
	fmt.Println(t)
	fmt.Printf("%02d.%02d.%04d\n", t.Day(), t.Month(), t.Year())

	t = time.Now().UTC()
	fmt.Println(t)
	fmt.Println(time.Now())

	week = 60*60*24*7*1e9
	week_from_now := t.Add(week)
	fmt.Println(week_from_now)

	fmt.Println(t.Format(time.RFC822))
	fmt.Println(t.Format(time.ANSIC))
	fmt.Println(t.Format("02 Jan 2021 12:23"))
	s := t.Format("2021.04.02")
	fmt.Println(t, "=>", s)
}
```

使用```go run```命令，运行输出：

```Golang
command-line-arguments
2021-04-22 11:21:52.6795288 +0800 CST m=+0.003927601
22.04.2021
2021-04-22 03:21:53.0697745 +0000 UTC
2021-04-22 11:21:53.0697745 +0800 CST m=+0.394173901
2021-04-29 03:21:53.0697745 +0000 UTC
22 Apr 21 03:21 UTC
Thu Apr 22 03:21:53 2021
22 Apr 22224 422:223
2021-04-22 03:21:53.0697745 +0000 UTC => 22224.21.22
```

很明显，后面两句输出的年、月、日何时间是不正确的：

```
22 Apr 22224 422:223
2021-04-22 03:21:53.0697745 +0000 UTC => 22224.21.22
```

要修正这个问题，需要将后面两行代码修改为：

```Golang
	fmt.Println(t.Format("02 Jan 2006 15:04"))
	s := t.Format("2006.04.02")
	fmt.Println(t, "=>", s)
```

但这个应该是属于```time```模块的BUG。

## 二、Go语言指针与引用

### 2.1 指针与引用的矛盾点

Go语言定义了引用和指针的概念，但是在函数调用时，如果是基本类型的引用调用，函数体中必须使用```*```来引用变量值，否则会导致编译错误：

```Golang
func main() {
	var val int = 10
	functionA(&val)
}

func functionA(a *int){
	/* a = 20是不被允许的，会导致编译错误 */
	*a = 20
}
```

从这一点来讲，引用传递的概念和其他语言存在差异与不同，也和普通引用就是别名的概念冲突。

但如果传入的是数组、切片或者结构体作为参数，函数体中则可以不用```*```来引用变量值，又和引用的概念复合；

```Golang
type mystruct struct {
	var a int
	var b int
}

func main() {
	var ms mystruct
	var num1 = make([]int, 0, 5)
	ms.a = 1
	ms.b = 2
	functionA(&ms)
	functionB(&num1)
}

func functionA(ms *mystruct) {
	(*ms).a = 10  /* 这是正确的使用方法 */
	ms.b = 20     /* 这样使用也是可以的 */
}

func functionB(arr []int) {
	(*arr)[2] = 10  /* 这是正确的使用方法 */
	arr[3] = 20     /* 这样使用也是可以的 */
}
```

合在一起，就能发现这种概念的混用和冲突点，非常的不好理解。

### 2.2 个人推荐解法

Go语言内部实现了垃圾回收机制，对语言使用者来讲，只用引用就好了，可以不用指针。使用的展现来讲也可以参考```C++```的风格：

```Golang
func main() {
	var val int = 10
	fmt.Println(val)
	functionA(val)
	fmt.Println(val)
}

func functionA(a &int){
	a = 20
}
```

这样即屏蔽了应用者指针的概念，内存的使用等，也统一了不同语言间的概念，更容易理解和语言切换。同时也不会存在Go语言自己类型的互相矛盾。

## 三、Go语言变量定义和初始化的疑惑

Go语言变量声明时，一会儿```type```在左边，一会儿在右边，有时候又可以省略，有没有什么规律？

### 3.1 规律总结

- 其实Go语言的任何类型变量的声明格式都是：

> var identifier type

- 如果要在声明的同时进行初始化，其格式都是：

> var identifier type = type{init}

其中```type```可以是基本类型、数组、切片、```map```等等，```Go```语言将```[]```、\*都算作类型。```init```为初始化值列表，对于基本数据类型，可以直接写值。

- 我们可以使用```:=```代替```=```，从而省去（声明并初始化语句）左边的```var```关键字，以及```type```类型说明。

### 3.2 规律证明

1. 常量声明与初始化（```const```关键字替换```var```关键字）

> const identifier [type] = value

2. 变量声明

> var identifier type

比如如下```C```与```GO```指针声明的差异性：

```Go
var a, b *int // a和b都是指向int类型的指针
int *a, b  // C定义方法，a是指针，b是整数
```

3. 基本类型变量定义与初始化

> var identifier type = value

```Go
var b bool = false
var n int16 = 34
var c1 complex64 = 5 + 10i
var ch byte = 65
var str string = "This is an exmaple of a string"
```

4. 数组的声明与初始化

> var identifier [len]type

```Go
var arrAge = [5]int{17, 12, 34, 23, 15}
var arrLazy = [...]int{17, 12, 34, 23, 15}
var arrKeyValue = [5]string{3: "Chris", 4: "Ron"}
arrKeyValue := [5]string{3: "Chris", 4: "Ron"}
```

5. 切片的声明与初始化

> var identifier []type //（不需要说明长度）

```Go
var slice1 []type = make([]type, len, cap)
var slice1 []type = arr1[start:end]
slice1 := make([]type, len, cap)
```

6. map的声明与初始化

```map```的声明：

> var map1 map[keytype]valuetype

```Go
var map1 map[string]int
mapcreated := make(map[string]float32)
mapLit = map[string]int{"one":1, "two":2}
```

```map```使用```make```声明：

> var map1[keytype]valuetype = make(map[keytype]valuetype)。

或者简写为：

> map1 := make(map[keytype]valuetype)

## 四、```new()```与```make()```的区别

```new()```和```make()```看起来没有什么区别，都在堆上分配内存，但是它们的行为不同，适用于不同的类型：

- ```new(T)```为每个新的类型T分配一片内存，初始化为 0 并且返回类型为*T的内存地址：这种方法返回一个指向类型为 T，值为 0 的地址的指针，它适用于值类型如数组和结构体，它相当于 &T{}；
- ```make(T)```返回一个类型为 T 的初始值，它只适用于3种内建的引用类型：**切片、map 和 channel**。

换言之，```new()```函数分配内存，```make()```函数初始化。


## 五、函数和方法的区别

- 函数将变量作为参数：Function1(recv)

- 方法在变量上被调用：recv.Method1()

不要忘记 Method1 后边的括号 ()，否则会引发编译器错误：```method recv.Method1 is not an expression, must be called```。

在接收者是指针时，方法可以改变接收者的值（或状态），这点函数也可以做到（当参数作为指针传递，即通过引用调用时，函数也可以改变参数的状态）。

receiver_type 叫做 （接收者）基本类型，这个类型必须在和方法同样的包中被声明。

在 Go 中，（接收者）类型关联的方法不写在类型结构里面，就像类那样；耦合更加宽松；类型和方法之间的关联由接收者来建立。

**方法没有和数据定义（结构体）混在一起：它们是正交的类型；表示（数据）和行为（方法）是独立的**。


