# Sublime

1. 到官网下载最新版本：http://www.sublimetext.com/
2. 选择```Tools->Install Package Control...```，安装```Package Control```工具；https://packagecontrol.io/installation#ST3
3. 重启Sublime，在Preferences下就出现了Package Control选型；
4. 选择该选项，输入install，会出现Install Package，选中回车会出现插件安装包的对话框。
5. 这里输入gosublime和go build，安装这两个插件即可。
6. 进入Install Package，安装ctags插件，把Mouse Bindings-Default内容复制到Mouse Bindings-User中，重启Sublime，即可支持跨文件之间的函数跳转。
