package main

import "fmt"

func main() {
    var a int = 20
    var ip *int
    var ipp **int

    if(nil == ip) {
        fmt.Println("addr ip is nil")
    }

    ip = &a
    ipp = &ip

    fmt.Printf("a addr : %x\n", &a)
    fmt.Printf("a addr : %x\n", ip)
    fmt.Printf("a value: %d\n", a)
    fmt.Printf("a value: %d\n", *ip)

    fmt.Printf("a addr : %x\n", ip)
    fmt.Printf("ip addr: %x\n", ipp)
    fmt.Printf("a value: %d\n", **ipp)

/*****************************************/
    b := [3]int{10, 100, 1000}
    var ptr [3]*int         /* 指针数组 */
    var ptrb *[3]int = &b   /* 数组指针 */

    for i := 0; i < 3; i++ {
        ptr[i] = &b[i]
        fmt.Printf("b[%d] = %d\n", i, *ptr[i])
    }

    for i := 0; i < 3; i++ {
        fmt.Printf("b[%d] = %d\n", i, (*ptrb)[i])
    }

/*****************************************/
    var x int = 3
    var y int = 5

    fmt.Printf("x = %d, y = %d\n", x, y)
    swap(&x, &y)
    fmt.Printf("x = %d, y = %d\n", x, y)

/*****************************************/
    add := func(x, y int) int {
        return (x + y)
    }

    sub := func(x, y int) int {
        return (x - y)
    }

    value := calc(3, 5, add)
    fmt.Printf("3 + 5 = %d\n", value)
    value = calc(8, 5, sub)
    fmt.Printf("8 - 5 = %d\n", value)
}

func swap(x *int, y *int) {
    var temp int
    temp = *x
    *x = *y
    *y = temp
}

type myfunc func(int, int) int

func calc(a, b int, mf myfunc) int {
    ret := mf(a, b)
    return ret
}

