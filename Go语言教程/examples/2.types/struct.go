package main

import "fmt"

type Books struct {
    title string
    author string
    book_id int
}

func main() {
    fmt.Println(Books{"Go", "https://golang.google.cn/", 112323})
    fmt.Println(Books{title: "Go", author: "https://golang.google.cn/", book_id: 112323})
    fmt.Println(Books{title: "Go", book_id: 112323})

    var book Books

    book.title = "Go"
    book.author = "https://golang.google.cn/"
    book.book_id = 112323

    fmt.Printf("book title: %s\n", book.title)
    fmt.Printf("book author: %s\n", book.author)
    fmt.Printf("book id   : %d\n\n", book.book_id)

    printBook(book)
    printBook(book)
    pointBook(&book)
    pointBook(&book)
}

func printBook(book Books) {
    fmt.Printf("book title: %s\n", book.title)
    fmt.Printf("book author: %s\n", book.author)
    fmt.Printf("book id   : %d\n\n", book.book_id)

    book.author = "konishi"
}

func pointBook(book *Books) {
    fmt.Printf("book title: %s\n", (*book).title)
    fmt.Printf("book author: %s\n", (*book).author)
    fmt.Printf("book id   : %d\n\n", book.book_id)

    /* 奇葩的Go语法:成员的访问只有'.',不区分结构体变量和结构体指针变量对成员变量的访问 */
    book.author = "konishi"
}

