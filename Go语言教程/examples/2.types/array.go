package main

import "fmt"

func main() {
    var i, j int
    var n[10] int

    /* init array */
    for i = 0; i < 10; i++ {
        n[i] = i + 100
    }

    for j = 0; j < 10; j++ {
        fmt.Printf("Element[%d] = %d\n", j, n[j])
    }

    change_element(n)
    for j = 0; j < 10; j++ {
        fmt.Printf("Element[%d] = %d\n", j, n[j])
    }

/******************************************************/
    balance := [5]float32{1000.0, 2.0, 3.0, 5.0, 35.0}
    for i = 0; i < 5; i++ {
        fmt.Printf("balance[%d] = %f\n", i, balance[i])
    }

    balance1 := [...]float32{1000.0, 2.0, 3.0, 5.0, 35.0}
    for j = 0; j < 5; j++ {
        fmt.Printf("balance1[%d] = %f\n", j, balance1[j])
    }

    balance2 := [5]float32{1:2.0, 3:7.0}
    for k := 0; k < 5; k++ {
        fmt.Printf("balance2[%d] = %f\n", k, balance2[k])
    }

/******************************************************/
    /* 创建一个空二维数组 */
    values := [][]int{}

    row1 := []int{1, 2, 3}
    row2 := []int{4, 5, 6}
    /* 使用append()函数向空二维数组添加两行一维数组 */
    values = append(values, row1)
    values = append(values, row2)

    fmt.Println("Row 1")
    fmt.Println(values[0])
    fmt.Println("Row 2")
    fmt.Println(values[1])


    a := [3][4] int {
        {0, 1, 2, 3},
        {4, 5, 6, 7},
        {8, 9, 10, 11},
    }

    for i = 0; i < 3; i++ {
        for j = 0; j < 4; j++ {
            fmt.Printf("a[%d][%d] = %d\n", i, j, a[i][j])
        }
    }

    animals := [][]string{}

    r1 := []string{"fish", "shark", "eel"}
    r2 := []string{"bird"}
    r3 := []string{"cat", "dog"}

    animals = append(animals, r1)
    animals = append(animals, r2)
    animals = append(animals, r3)

    for a := range animals {
        fmt.Printf("Row: %v\n", a)
        fmt.Println(animals[a])
    }

    not_change_array(animals)
    for a := range animals {
        fmt.Printf("Row: %v\n", a)
        fmt.Println(animals[a])
    }
}

/* This will not change array value */
func change_element(n[10] int) {
    n[4] = 4
}

func not_change_array(array [][]string) {
    row := []string{"man", "woman"}
    array = append(array, row)
}

