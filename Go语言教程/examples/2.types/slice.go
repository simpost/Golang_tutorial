package main

import "fmt"

func main() {
    var emp []int  /* empty slice */
    var num1 = make([]int, 0, 5)  /* init */
    var num2 = make([]int, 3, 5)  /* init */
    printSlice(emp)
    printSlice(num1)
    printSlice(num2)

    /* 切片截取 */
    numbers := []int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
    fmt.Println("numbers =", numbers)
    fmt.Println("numbers[1:4] =", numbers[1:4])
    fmt.Println("numbers[:3] =", numbers[:3])
    fmt.Println("numbers[4:] =", numbers[4:])

    numbers1 := numbers[2:5]
    printSlice(numbers1)

    /* append() and copy() */
    var number []int
    printSlice(number)
    number = append(number, 0)
    printSlice(number)
    number = append(number, 1)
    printSlice(number)
    number = append(number, 2, 3, 4)
    printSlice(number)

    number1 := make([]int, len(number), 2*cap(number))
    copy(number1, number)
    printSlice(number1)
}

/* len() and cap() */
func printSlice(x []int){
    fmt.Printf("len = %d cap = %d slice = %v\n", len(x), cap(x), x)
}

