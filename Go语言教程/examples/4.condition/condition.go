package main

import "fmt"

func main() {
    var a int = 10
    var b int = 20

    if a < 20 {
        fmt.Println("a less than 20")
        if b == 20 {
            fmt.Println("b equal to 20")
        }
    } else {
        fmt.Println("a greater than 20")
    }
    fmt.Println("the value of a :", a)
    fmt.Println("the value of b :", b)


    var marks int = 90
    var grade string = "B"

    switch marks {
        case 90: grade = "A"
        case 80: grade = "B"
        case 50:
            fallthrough
        case 60, 70: grade = "C"
        default: grade = "D"
    }

    switch {
        case grade == "A":
            fmt.Println("优秀!")
        case grade == "B":
            fmt.Println("良好")
        case grade == "C":
            fmt.Println("及格")
        case grade == "D":
            fmt.Println("不及格")
        default:
            fmt.Println("差")
    }


    var x interface{}

    switch x.(type) {
        case nil:
            fmt.Println("The type of x: pointer")
        case int:
            fmt.Println("The type of x: int")
        case float64:
            fmt.Println("The type of x: float64")
        case func(int) float64:
            fmt.Println("The type of x: func(int) float64")
        case bool, string:
            fmt.Println("The type of x: bool or string")
        default:
            fmt.Println("The type doesn't know")
    }


    var c1, c2, c3 chan int
    var i1, i2 int

    select {
        case i1 = <-c1:
            fmt.Println("Received ", i1, " from c1")
        case c2 <- i2:
            fmt.Println("Send ", i2, " to c2")
        case i3, ok := <-c3:
            if ok {
                fmt.Println("Receive ", i3, " from c3")
            } else {
                fmt.Println("c3 is closed")
            }
        default:
            fmt.Println("no communication")
    }
}

