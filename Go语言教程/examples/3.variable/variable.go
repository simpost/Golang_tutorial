package main
import "fmt"

/* In general, this is used for global variable */
var (
    x = 19
    y = 38
)

/* Only used in functions */
//g, h := 123, "haha"

func main() {
    var a string = "Runoob"
    fmt.Println(a)

    var b, c int = 1, 2
    fmt.Println(b, c)

    var d int
    var e bool
    fmt.Println(d, e)

/*****************************/
    var intval = 13
    fmt.Println(intval)

    hello, world, count := "hello", "world", 20
    fmt.Println(hello, world, count)

    var radius float32 = 5.0
    const Pi float32 = 3.141592653
    fmt.Println("area:", Pi*radius*radius)

    var r int = 5
    var area = Pi*float32(r)*float32(r)
    fmt.Println("area:", area)

/*****************************/
    const (
        i = iota   // 0
        j          // 1
        k = "ha"   // 独立值, iota += 1
        l          // "ha", iota += 1
        m = 100    // iota += 1
        n          // 100, iota += 1
        o = iota   // 6, iota += 1
        p          // 7
    )
    fmt.Println(i, j, k, l, m, n, o, p)

/*****************************/
    const (
        x = iota    // reset, 0 again
        y
        z
    )
    fmt.Println(x, y, z)

/*****************************/
    const (
        x1 = 1<<iota    // 1<<0 = 1
        x2 = 3<<iota    // 3<<1 = 6
        x3              // 3<<2 = 12
        x4              // 3<<3 = 24
    )
    fmt.Println(x1, x2, x3, x4)
}
