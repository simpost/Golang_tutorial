package main

import (
    "fmt"
    "math"
)

type Circle struct {
    radius float64
}

/* 声明一个函数类型 */
type callback_t func(int) int

func main() {
    /* 声明函数变量 */
    getSquareBoot := func(x float64) float64 {
        return math.Sqrt(x)
    }
    fmt.Println(getSquareBoot(9))

    /* nextNumber是一个函数 */
    nextNumber := getSquence()
    fmt.Println(nextNumber())
    fmt.Println(nextNumber())
    fmt.Println(nextNumber())

    nextNumber1 := getSquence()
    fmt.Println(nextNumber1())
    fmt.Println(nextNumber1())

    /* 回调方法实现 */
    testCallBack(1, callBack)
    testCallBack(2, func(x int) int{
        fmt.Println("I'm callback2, x:", x)
        return x
    })

    /* 调用方法 */
    var c1 Circle
    c1.radius = 10.00
    fmt.Println("Circle area:", c1.getArea())
}

/* 闭包:匿名函数 */
func getSquence() func() int {
    i := 0
    return func() int {
        i++
        return i
    }
}

func testCallBack(x int, f callback_t) {
    f(x)
}

func callBack(x int) int {
    fmt.Println("I'm callback1, x:", x)
    return x
}

/* 定义Circle的方法 */
func (c Circle) getArea() float64 {
    return 3.14 * c.radius * c.radius
}

