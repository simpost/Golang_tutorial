package main

import "fmt"

func main() {
    var a int = 100
    var b int = 200
    var ret int

    no_return()

    ret = max(a, b)
    fmt.Println("The max is :", ret)

    c, d := swap_int(a, b)
    fmt.Println("origin:", a, b)
    fmt.Println(" swap :", c, d)

    x, y := swap_string("Google", "Facebook")
    fmt.Println("origin: Google Facebook")
    fmt.Println(" swap :", x, y)
}

func max(num1, num2 int) int {
    var result int
    if num1 > num2 {
        result = num1
    } else {
        result = num2
    }
    return result
}

// This funcion has no return value
func no_return() {
    fmt.Println("no return")
}

// the same name function was not allowed in go, error: duplicate define swap
//func swap_int(x, y int) (int, int) {
//    return y, x
//}

func swap_int(x, y int) (a, b int) {
    a = y
    b = x
    return
}

func swap_string(x, y string) (string, string) {
    return y, x
}

