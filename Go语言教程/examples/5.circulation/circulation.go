package main

import "fmt"

func main() {
    sum := 1

    for ; sum <= 10; {
        sum += sum
    }
    fmt.Println("sum = ", sum)


    strings := []string{"google", "runoob", "facebook"}

    for i, s := range strings {
        fmt.Println(i, s)
    }


    numbers := [6]int{1, 2, 3, 5}
    for j, x := range numbers {
        fmt.Println(j, x)
    }


    var n, m int
    /* 不使用标记 */
    fmt.Println("break")
    for n = 2; n < 100; n++ {
        for m = 2; m <= (n/m); m++ {
            if(n % m == 0) {
                break;
            }
        }
        if(m > (n/m)) {
            fmt.Println(n, "是素数")
        }
    }

    /* 使用标记 */
    fmt.Println("break repeat:")
repeat:
    for n = 2; n < 100; n++ {
        for m = 2; m <= (n/m); m++ {
            if(n % m == 0) {
                fmt.Println("n % m == 0")
                break repeat
            }
        }
    }
    fmt.Println("end")
}

