# Golang

![images](images/golang.jpg)

Go语言是编译语言设计的又一次尝试，是对类C语言的重大改进，它不但能让你访问底层操作系统，还提供了强大的**网络编程**和**并发编程**支持。Go语言的用途众多，可以进行网络编程、系统编程、并发编程、分布式编程等。

> C、C++、Go等都是编译型语言，需要编译后才能运行，且Go自带编译器，无需单独安装；Python、Ruby、JavaScript等语言是解释型语言，在运行时动态翻译/编译。因此编译型语言执行效率高，但缺点是跨平台能力弱，不便于调试；解释型语言跨平台能力强，易于调试，但执行速度慢。

Go语言旨在不损失应用程序性能的情况下降低代码的复杂性，具有“部署简单、并发性好、语言设计良好、执行性能好”等优势。

Go从C语言继承了相似的表达式语法、控制流结构、基础数据类型、调用参数传值、指针等很多思想，还有C语言一直所看中的编译后机器码的运行效率以及和现有操作系统的无缝适配。

Go语言没有类和继承的概念，Go通过接口（interface）来实现多态，因此与Java或C\+\+并不相同。Go语言有一个清晰易懂的轻量级类型系统，在类型之间也没有层级之说。因此可以说Go语言是一门混合型的语言。

## 一、Go语言结构

Go语言的基础组成有如下几个部分：

- 包声明：关键字```package```，其中```package main```表示可独立执行的Go程序；
- 引入包：关键字```import```；
- 函数：关键字```func```，其中```func main()```是每一个可执行程序所必须包含的；程序启动后的执行是init() -> main()；
- 变量：关键字```var```，变量名由字母、数字、下划线组成，其中首个字符不能为数字；‘_’为只写变量，不能得到其值；
- 语句 & 表达式：执行程序逻辑和数据处理；
- 注释：单行注释用‘//’，多行注释用‘/\*\*/’，且注释不可嵌套。

当标识符（包括常量、变量、类型、函数名、结构字段等）以一个***大写字母开头***（如Group1），那么使用这种形式的标识符对象可以被外部包代码所使用（客户端程序需要先导入这个包），这被称为**导出**（像面向对象语言的public）；标识符如果以小写字母开头，则对包外是不可见的，但在整个包内部是可见并且可用的（像面向对象语言中的protected）。

Go语言标准库常用的包及功能

标准库包名 | 功能
--- | ---
bufio | 带缓冲的I/O操作
bytes | 字节操作
container | 封装堆、列表、环形队列表等容器
crypto | 加密算法
database | 数据库驱动和接口
debug | 各种调试文件格式访问及调试功能
encoding | 常见算法如JSON、XML、Base64等
flag | 命令行解析
fmt | 格式化操作
go | Go语言的词法、语法树、类型等，可以通过这个包进行代码信息提取和修改
html | HTML转移及模板系统
image | 常见图形格式的访问及生成
io | 实现I/O原始访问接口及访问封装
math | 数学库
net | 网络库，支持socket、http、邮件、rpc、smtp等
os | 操作系统平台，不依赖平台操作的封装
path | 兼容各操作系统的路径操作封装
plugin | Go 1.7加入的插件系统，支持代码编译为插件，按需加载
reflect | 反射支持，可以动态获得代码中的类型信息，获取和修改变量的值
regexp | 正则表达式封装
runtine | 运行时接口
sort | 排序接口
strings | 字符串转换、解析及实用函数
time | 时间接口
text | 文本模板及```Token```词法器

## 二、Go语言基础语法

- Go标记：Go程序由多个标记组成，标记可以是关键字、标识符、常量、字符串、符号等。
- 行分隔符：在Go程序中，一行代表一个语句结束，不需要分号‘;’结尾；如果多个语句写在一行，才使用分号‘;’间隔。
- 注释：单行注释用‘\/\/’，多行注释用‘\/\*\*\/’，注释不可嵌套。
- 标识符：标识符用来命名变量、类型等程序实体，一个标识符实际上就是一个或多个字母（```A~Z```和```a~z```）、数字（```0~9```）、下划线（_）组成的序列，但第一个字符必须是字母或下划线而不能是数字。
- 字符串连接：Go语言的字符串可以通过```+```实现连接。
- 空格：Go语言中变量的声明必须使用空格隔开，如：```var age int```；最好在变量和运算符之间加入空格，使程序更加美观。
- 关键字：下面是常见的25个Go语言关键字：

```Golang
break    default      func    interface    select
case     defer        go      map          struct
chan     else         goto    package      switch
const    fallthrough  if      range        type
continue for          import  return       var
```

另外，Go语言还定义了36个预定义标识符：

```Golang
append    bool    byte    cap      close    complex    complex64     complex128    uint16
copy      false   float32 float64  imag     int        int8          int16         uint32
int32     int64   iota    len      make     new        nil           panic         uint64
print     println real    recover  string   true       uint          uint8         uintptr
```

- 程序一般由关键字、常量、变量、运算符、类型和函数组成。
- 程序中可能会使用到这些分隔符：括号 ()，中括号 [] 和大括号 {}。
- 程序中可能会使用到这些标点符号：‘.’、‘,’、‘;’、‘:’ 和 ‘…’。

## 三、Go语言程序执行

直接执行：

```Golang
$ go run hello.go
hello, world!
```

编译后执行：

```Golang
$ go build hello.go
$ ls
hello hello.go
$ ./hello
hello, world!
```

## 四、Go语言数据类型

***数据类型的出现是为了把数据分成所需内存大小不同的数据***，编程的时候需要用大数据的时候才需要申请大内存，就可以充分利用内存。

### 4.1 按数据类型分类

序号 | 类型 | 描述
--- | --- | ---
1 | 布尔类型 | 布尔型的值只可以是常量 true 或者 false。如var b bol = true。
2 | 数字类型 | Go 语言支持整型和浮点型数字，并且支持复数，其中位的运算采用补码。
3 | 字符串类型 | 字符串就是一串固定长度的字符连接起来的字符序列。Go的字符串是由单个字节连接起来的。Go语言的字符串的字节使用UTF-8编码标识Unicode文本。
4 | 派生类型 | 包括：<br><ul><li> a) 指针类型（Pointer） </li><li> b) 数组类型 </li><li> c) 结构化类型（struct） </li><li> d) Channel类型 </li><li> e) 函数类型 </li><li> f) 切片类型 </li><li> g) 接口类型（interface） </li><li> h) Map类型 </li><li> i) Channel类型

### 4.2 数字类型包括

序号 | 类型 | 描述
--- | --- | ---
1 | uint8 | 无符号 8 位整型 (0 到 255)
2 | uint16 | 无符号 16 位整型 (0 到 65535)
3 | uint32 | 无符号 32 位整型 (0 到 4294967295)
4 | uint64 | 无符号 64 位整型 (0 到 18446744073709551615)
5 | int8 | 有符号 8 位整型 (-128 到 127)
6 | int16 | 有符号 16 位整型 (-32768 到 32767)
7 | int32 | 有符号 32 位整型 (-2147483648 到 2147483647)
8 | int64 | 有符号 64 位整型 (-9223372036854775808 到 9223372036854775807)

### 4.3 浮点类型包括

序号 | 类型 | 描述
--- | --- | ---
1 | float32 | IEEE-754 32位浮点型数
2 | float64 | IEEE-754 64位浮点型数
3 | complex64 | 32 位实数和虚数
4 | complex128 | 64 位实数和虚数

### 4.4 其他类型包括

序号 | 类型 | 描述
--- | --- | ---
1 | byte | 类似uint8
2 | rune | 类似int32
3 | uint | 32或64位
4 | int  | 与uint一样大小的有符号数
5 | uintptr | 无符号整型，用于存放一个指针

### 4.5 数据类型转换

类型转换用于将一种数据类型的变量转换为另外一种类型的变量。Go语言类型转换基本格式如下：

> type_name(expression)

```type_name```为类型，```expression```为表达式。

```Golang
var sum int = 17
var count int = 5
ver mean float32

mean = float32(sum)/float32(count)
```

## 五、Go语言派生数据类型

### 5.1 数组类型

#### 5.1.1 一维数组

数组是具有相同类型的一组编号且长度固定的数据项序列。与C语言类似，数组元素可以通过索引（位置）来读取（或者修改），索引从0开始，也即第一个元素的索引为0，第二个元素索引为1，以此类推。

![images](images/array.png)

1. 声明数组

Go语言声明数组时需要指定元素类型及元素个数，语法格式如下：

```Golang
var variable_name [SIZE] variable_type
```

2. 初始化数组

数组可以在定义的时候初始化：

```Golang
var balance = [5]float32{1000.0, 2.0, 3.4, 6.0, 55.0}
/* 或者 */
balance := [5]float32{1000.0, 2.0, 3.4, 6.0, 55.0}
```

在初始化时，如果数组长度不确定，可以使用```...```代替数组长度，编译器会根据元素个数自行推断数组的长度：

```Golang
var balance = [...]float32{1000.0, 2.0, 3.4, 6.0, 55.0}
/* 或者 */
balance := [...]float32{1000.0, 2.0, 3.4, 6.0, 55.0}
```

如果设置了数组长度，我们可以通过指定下标来初始化指定元素：

```Golang
balance := [5]float32{1:2.0, 3:7.0}
```

注意：初始化数组```{}```中的元素个数不能大于```[]```中的数字，否则会报错；如果忽略```[]```中的数字不设置数组大小(使用```...```)，Go语言会根据元素个数来设置数组大小。

3. 访问数组元素

数组元素可以通过索引（位置）来读取（或修改）。格式为数组名后加中括号，中括号中为索引值。比如下面获取了```balance```数组中的第10个元素的值：

> var salary float32 = balance[9]

#### 5.1.2 多维数组

多维数组的声明方法如下：

> var variable_name [SIZE1][SIZE2]...[SIZEN] variable_type

其中二维数组是最简单的多维数组，其本质上是由一维数组组成的。二维数组中的元素可通过a[i][j]来访问。

多维数组可以通过大括号的形式初始化，比如下面初始化了一个3行4列的二维数组：

```Golang
a := [3][4]int {
    {0，1，2，3}，
    {4，5，6，7}，
    {8，9，10，11}，
}
```

访问二维数组与一维数组类似，只是需要同时指定行索引和列索引。

#### 5.1.3 向函数传递数组

如果需要向函数传递数组，在函数定义时，形参需要声明为数组：

```Golang
func myFunction(param [10]int) {
    ... ...
}
/* 或不指定数组大小 */
func myFunction(param []int) {
    ... ...
}
```

需要注意：**与C语言不同，在函数中修改数组元素的值，并不会修改到实参中的值！要实现实参的修改，请使用地址传递，或者使用切片类型**。

### 5.2 指针类型

1. 什么是指针

我们知道，变量是一种方便使用内存的占位符，用于引用计算机的内存地址。在变量前使用取地址运算符‘&’，可以得到该变量的内存地址：

```Golang
var a int = 10
fmt.Printf("addr: %x\n", &a)
```

类似于变量和常量，在使用指针前需要声明指针，格式如下：

```Golang
var ip *int       /* 指向整型 */
var fp *float32   /* 指向浮点型 */
```

2. 如何使用指针

指针的使用流程如下：
- 定义指针变量；
- 为指针变量赋值；
- 访问指针变量中指向地址的值。

我们需要在指针类型变量前面加上‘*’来获取指针所指地址的内容。

```Golang
    var a int = 20
    var ip *int = &a

    fmt.Printf("a addr : %x\n", &ip)
    fmt.Printf("a value: %d\n", *ip)
```

3. 空指针

空指针为```nil```，它和其他语言的null、None、nil、NULL一样，都指代零值或空值。

4. 指针数组与数组指针

- 指针数组实际是一个数组，数组里每一个元素都是指针；
- 数组指针实际是一个指针，它指向的是一个数组的地址。

```Golang
    b := [3]int{10, 100, 1000}
    var ptr [3]*int         /* 指针数组 */
    var ptrb *[3]int = &b   /* 数组指针 */

    for i := 0; i < 3; i++ {
        ptr[i] = &b[i]
        fmt.Printf("b[%d] = %d\n", i, *ptr[i])
    }

    for i := 0; i < 3; i++ {
        fmt.Printf("b[%d] = %d\n", i, (*ptrb)[i])
    }
```

5. 函数指针与指针函数

与C语言类似：
- 函数指针实际是一个指针，该指针指向一个函数；
- 指针函数实际是一个函数，该函数返回一个指针。

```Golang
type myfunc func(int, int) int

func calc(a, b int, mf myfunc) int {
    ret := mf(a, b)
    return ret
}

func main(){
    add := func(x, y int) int {
        return (x + y)
    }

    sub := func(x, y int) int {
        return (x - y)
    }

    value := calc(3, 5, add)
    fmt.Printf("3 + 5 = %d\n", value)
    value = calc(8, 5, sub)
    fmt.Printf("8 - 5 = %d\n", value)
}
```

6. 指向指针的指针

如果一个指针变量存放的又是另一个指针变量的地址，则称这个指针变量为指向指针的指针变量。

当定义一个指向指针的指针变量时，第一个指针存放第二个指针的地址，第二个指针存放变量的地址：

![images](images/double_pointer.png)

指向指针的指针声明格式如下：

> var ptr **int

访问指向指针的指针所指向真是变量的值，需要使用两个‘*’号。


### 5.3 结构体类型

数组只能存储同一类型的数据，而结构体可以通过定义不同的数据类型实现不同类型数据的存储。

1. 结构体定义

结构体定义需要使用```type```和```struct```语句，格式如下：

```Golang
type struct_variable_type struct {
    member1 definition
    member2 definition
    ... ...
    membern definition
}

variable_names := struct_variable_type {value1, value2, ..., valuen}
variable_names := struct_variable_type {key1:value1, key2:value2, ..., keyn:valuen}
```

2. 访问结构体成员

结构体成员的方位，使用点号‘.’操作符：

> 结构体变量.成员名

注意：**在Go语言中，不区分结构体变量与结构体指针变量对结构体成员的访问方法**。

```Golang
type Books struct {
    title string
    author string
    book_id int
}

    var book Books
    var bookp *Books = &book

    book.title = "Go"
    book.author = "https://golang.google.cn/"
    book.book_id = 112323

    bookp.author = "konishi"     /* this is OK */
    (*bookp).author = "konishi"  /* this is OK, too */
```

3. 使用结构体

- 你可以像普通数据类型一样，将结构体类型作为参数传递给函数，默认使用的值传递，为了提高效率，可以使用指针；
- 你可以像普通数据类型一样，定义结构体数组，数组中的所有成员的数据类型都是结构体；
- 你可以像普通数据类型一样，为结构体定义指针或指向结构体指针的指针，使用指针访问结构体成员时，使用```struct_pointer.title```。

### 5.4 切片类型（Slice）

Go语言中，切片是对数组的抽象（动态数组）。数组的长度不可以改变，这在特定场景中并不合适。切片的长度是不固定的，可以动态追加元素。

1. 切片定义

声明一个未指定大小的数组来定义切片，切片不需要指定长度：

> var identifier []type

也可以使用```make()```函数来创建切片：

```Golang
var slice1 []type = make([]type, len, [capacity])
/* or */
slice1 := make([]type, len, [capacity])
```

其中，```len```是是数组的长度也是切片的初始长度；```capacity```是可选参数，用于指定切片的容量。

2. 切片初始化

- 可以像数组一样，直接初始化：

```Golang
s := []int{1, 2, 3}
```

```[]```表示切片类型，```{1, 2, 3}```初始化```s```，其中```cap=len=3```。

- 下面将下标```startIndex```到```endIndex-1```的元素创建为一个切片：

> s := array[startIndex:endIndex]

默认```endIndex```时表示一直到```array```的最后一个元素，默认```startIndex```时，表示从```array```的第一个元素开始。因此，下面初始化切片为数组的引用：

> s := array[:]

3. 切片的使用

- 未初始化之前切片默认为nil，长度也为0；
- 可以在数组中设置下限和上限来截取切片，下限与上限使用冒号‘:’分割。
- 可以通过```len()```方法获取长度，也可通过```cap()```函数获取最大容量；
- 可以使用```append()```方法向切片追加新元素，也可以通过```copy()```方法拷贝切片元素。


### 5.5 集合类型（Map）

```Map```是一种存储无序和值无需的键值对集合。```Map```最重要的一点是通过```key```来快速检索数据，```key```类似于索引，指向数据的值。Map是一种集合，我们可以像迭代数组、切片一样来迭代```Map```。但因为```Map```是无序的，我们无法决定它的返回顺序。

1. 集合的定义

可以使用```make```或```map```关键字来定义集合：

```Golang
/* 声明变量，默认map是nil的 */
var map_variable map[key_data_type]value_data_type
/* 使用map函数 */
map_variable := make(map[key_data_type]value_data_type)
```

如果不初始化```Map```，则会创建一个```nil map```，```nil map```不能用来存放键值对。比如如果注释下面的```countryCapitalMap = make(map[string]string)```，是没法编译通过的。

```Golang
package main

import "fmt"

func main() {
    var countryCapitalMap map[string]string  /* 创建集合 */
    countryCapitalMap = make(map[string]string)

    countryCapitalMap ["France"] = "巴黎"
    countryCapitalMap ["Italy"]  = "罗马"
    countryCapitalMap ["Japan"]  = "东京"
    countryCapitalMap ["India"]  = "新德里"

    for country := range countryCapitalMap {
        fmt.Println(country, "首都是", countryCapitalMap [country])
    }

    capital, ok := countryCapitalMap ["American"]
    if ok {
        fmt.Println("American 的首都是:", capital)
    } else {
        fmt.Println("American 的首都不存在")
    }
}
```

2. 集合的使用

我们可以使用```delete()```函数来删除集合中指定的元素，参数为```map```和其对应的```key```。

> delete(map, key)

### 5.6 范围（range）

Go语言中```range```关键字用于```for```循环中迭代数组（array）、切片（slice）、通道（channel）或集合（Map）的元素。在数组和切片中，它返回元素的索引和索引对应的值，在集合中返回key-value对。

### 5.7 接口类型（interface）

Go语言提供了接口数据类型，它把所有具有共性的方法定义在一起，任何其他类型只要实现了这些方法就是实现了这个接口。

```Golang
/* 定义接口 */
type interface_name interface {
   method_name1 [return_type]
   method_name2 [return_type]
   method_name3 [return_type]
   ...
   method_namen [return_type]
}

/* 定义结构体 */
type struct_name struct {
   /* variables */
}

/* 实现接口方法 */
func (struct_name_variable struct_name) method_name1() [return_type] {
   /* 方法实现 */
}
...
func (struct_name_variable struct_name) method_namen() [return_type] {
   /* 方法实现*/
}
```

## 六、Go语言变量与常量

### 6.1 变量与声明

变量来源于数学，是计算机语言中能储存计算结果或能表示值抽象概念。变量可以通过变量名访问。

Go语言变量名由字母、数字、下划线组成，其中首个字符不能为数字。定义变量使用关键字```var```。Go语言声明变量有如下4种方法：

1. 指定变量类型，声明时不赋值，使用默认值：

```Golang
var identifier type
var identifier1, identifier2 type
```

2. 声明变量自动判断类型：

```Golang
var v_name = value
```

3. 使用```:=```声明并赋值变量，可省略```var```关键字（```:=```左侧变量如果是已声明的变量，就会产生编译错误），注意```:=```只能在函数体中使用：

```Golang
v_name := value
```

4. 多变量声明，可以是相同的数据类型，也可以是不同的数据类型：

```Golang
var vname1, vname2, vname3 type
vname1, vname2, vname3 = v1, v2, v3

var vname1, vname2, vname3 = v1, v2, v3

vname1, vname2, vname3 := v1, v2, v3

// 类型不同的多个变量声明，适用于全局变量，局部变量不能使用这种方式
var (
    vname1 v_type1
    vname2 v_type2
```

在使用Go变量时，需要注意以下几点：
- ```:=```只能用在函数体内，```var ()```只能用在函数体外（全局变量）。
- 声明变量时没有初始化，则默认为零值（数值为0，布尔为fals，字符串为""，其他类型为nil）。
- 如果你想要交换两个变量的值，则可以简单地使用```a, b = b, a```，注意两个变量的类型必须是相同。
- 空白标识符‘\_’为只写变量，不能得到其值，可用于抛弃值，如值5在```_, b = 5, 7```语句中就被抛弃。
- Go语言不允许只声明不使用的变量存在（单纯赋值也不行），否则会得到```a declared and not used```错误。

### 6.2 变量作用域

作用域为已声明标识符所表示的常量、类型、变量、函数或包在源代码中的作用范围。Go语言中变量可以在三个地方声明：

- 函数内定义的变量称为局部变量：其作用域只在函数体内，参数和返回值也是局部变量；
- 函数外定义的变量称为全局变量：其作用域可以在整个包甚至外部包（被导出后）使用；
- 函数定义中的变量称为形式参数：其将被作为函数局部变量来使用。

全局变量首字母大写时（被导出），可以被包外部使用。若全局变量和局部变量名称相同，在局部范围内，局部变量被优先考虑。

### 6.3 常量

常量是一个简单值的标识符，在程序运行时，不会被修改的量。常量的定义格式（常量需定义时赋值，type可以省略）：

```Golang
const identifier [type] = value
const identifier1, identifier2 [type] = value1, value2
```

- 常量表达式中，函数必须是内置函数，否则编译不过。
- 常量可以用```len()```, ```cap()```, ```unsafe.Sizeof()```函数计算表达式的值。
- 常量中的数据类型只可以是布尔型、数字型（整数型、浮点型和复数）和字符串型。

常量也可以用于定义枚举变量：

```Golang
const {
    Unknow = 0
    Female = 1
    Male   = 2
}
```

```iota```是特殊常量，可以认为是一个可以被编译器修改的常量。```iota```在每一个```const```关键字出现时将被重置为 0，然后在下一个```const```出现之前，每出现一次```iota```，其所代表的数字就会自动增加1(```iota```可理解为```const```语句块中的行索引)。```iota```常和```const```一起用于定义枚举值：

```Golang
const (
    a = iota
    b = iota
    c = iota
    d = iota
)
```

## 七、Go语言运算符

- 算术运算符：假定A为10，B为20

运算符 | 描述 | 实例
--- | --- | ---
\+ | 相加 | A + B输出30
\- | 相减 | A - B输出-10
\* | 相乘 | A * B输出200
/ | 相除 | B/A输出2
% | 求余 | B%A输出0
++ | 自增 | A++输出11
-- | 自减 | A--输出9

- 关系运算符：假定A为10，B为20

运算符 | 描述 | 实例
--- | --- | ---
== | 检查两个值是否相等，相等返回True，否则返回False | A == B为False
!= | 检查两个值是否不相等，相等返回False，否则返回True | A != B为True
\> | 检查左边值是否大于右边值，是返回True，否则返回False | A > B为False
< | 检查左边值是否小于右边值，是返回True，否则返回False | A < B为True
\>= | 检查左边值是否大于等于右边值，是返回True，否则返回False | A >= B为False
<= | 检查左边值是否小于等于右边值，是返回True，否则返回False | A <= B为True

- 逻辑运算符：假设A为True，B值为False

运算符 | 描述 | 实例
--- | --- | ---
&& | 逻辑AND运算符，真真为真，真假为假，假假为假 | A && B为False
\|\| | 逻辑OR运算符，真真为真，真假为真，假假为假 | A \|\| B为True
! | 逻辑NOT运算符，真为假，假为真 | !(A && B)为True

- 位运算符：假定A为60，B为13

运算符 | 描述 | 实例
--- | --- | ---
& | 对参与运算的两个数进行二进制位与 | A & B结果为12
\| | 对参与运算的两个数进行二进制位或 | A \| B结果为61
^ | 对参与运算的两个数进行二进制位异或 | A ^ B结果为49
<< | 将左值按位向左移动右值位 | A << 2结果为240
\>\> | 将左值按位向右移动右值位 | A \>\> 2结果为15

- 赋值运算符

运算符 | 描述 | 实例
--- | --- | ---
= | 将一个表达式的值赋值给左值 | C = A + B
+= | 相加后再赋值 | C += A等于C = C + A
-= | 相减后再赋值 | C -= A等于C = C - A
\*= | 相乘再赋值 | C \*= A等于C = C\*A
/= | 相除再赋值 | C /= A等于C = C/A
%= | 求余再赋值 | C %= A等于C = C%A
<<= | 左右后赋值 | C <<= 2等于C = C<<2
\>\>= | 右移后赋值 | C >>=2等于C = C>>2
&= | 按位与后赋值 | C &= 2等于C = C&2
\|= | 按位或后赋值 | C \|= 2等于C = C\|2
^= | 按位异或后赋值 | C ^= 2等于C = C^2

- 其他运算符

运算符 | 描述 | 实例
--- | --- | ---
& | 返回变量存储地址 | &a
\* | 指针变量 | \*a

- 运算符优先级

有些运算符拥有较高的优先级，二元运算符的运算方向均是从左至右。下表列出了所有运算符以及它们的优先级，由上至下代表优先级由高到低：

优先级 | 运算符
--- | ---
5 | *, /, %, <<, >>, &, ^
4 | +, -, \|, ^
3 | ==, !=, <, <=, >, >=
2 | &&
1 | \|\|

当然，你也可以通过使用括号来提升某个表达式的整体运算优先级。

## 八、Go语言逻辑控制

### 8.1 条件语句

条件语句需要指定一个或多个条件，通过条件匹配来决定执行指定的语句。

1. if语句

![images](images/if_condition.png)

- if语句由布尔表达式后紧跟一个或多个语句组成，布尔表达式为真时执行紧跟的语句，否则不执行；

```Golang
if 条件表达式 {
	// 在条件表达式为true时执行
}
```

- if语句后可选else语句，表示if语句中布尔表达式为false时执行；

```Golang
if 条件表达式 {
	// 在条件表达式为true时执行
} else {
	// 在条件表达式为false时执行
}

```

- 可以在if语句或else if语句中嵌入一个或多个if或else if语句；

```Golang
if 布尔表达式1 {
	// 在布尔表达式1为true时执行
	if 布尔表达式2 {
		// 在布尔表达式2为true时执行
	}
} else {
	// 在布尔表达式1为false时执行
}
```

2. switch语句

![images](images/switch_condition.png)

switch语句用于基于不同条件执行不同动作，每个case分支都是唯一的，从上至下逐一测试，指导匹配为止：
- 可以同时测试多个可能符合条件的值，使用逗号分割开即可；
- 类型不被局限于常量或整数，但必须是相同的类型，或者最终结果为相同类型的表达式；
- switch语句还可以被用于type-switch来判断某个interface变量中实际存储的变量类型；
- 默认case最后自带break语句，如果要连续执行后面的case，可以使用```fallthrough```；
- ```fallthrough```会强制执行后面的case语句，不会判断下一条case表达式结果是否为true；

```Golang
/* switch语法格式 */
switch var1 {
	case val1:
		...
	case val2:
		...
    case val3, val4, val5:
		...
		fallthrough
	case val6:
		...
	default: /* 可选 */
		...
}

/* type-switch语法 */
switch x.(type) {
	case nil:
		statement(s)
	case int:
		statement(s)
	case float64:
		statement(s)
	case bool, string:
		statement(s)
	case func(int) float64:
		statement(s)
	default: /* 可选 */
		statement(s)
}
```

3. select语句

select语句是Go中的一个控制结构，类似于通信场景的switch语句（每个case必须是通信操作，要么发送要么接收）；select会随机执行一个可运行的case，如果没有case可运行，它将阻塞，直到有case可运行：
- 每个case都必须是一个通信；
- 所有channel表达式都会被求值；
- 所有被发送的表达式都会被求直；
- 如果任意某个通信可以进行，它就执行，其他被忽略；
- 如果有多个case都可以运行，select会随机公平地选出一个执行，其他不会执行；
- 若上面的都不满足：如果有default子句，则执行该语句；如果没有default子句，select阻塞，直到某个通信可以运行；

```Golang
select {
	case communication clause :
		statement(s)
	case communication clause :
		statement(s)
	default: /* 可选 */
		statement(s)
}
```

### 8.2 循环语句

1. for语句

在不少实际问题中有许多具有规律性的重复操作，因此在程序中需要重复执行某些语句。Go语言只提供了```for```语句来实现循环。

![images](images/for_loop.png)


for循环可以执行指定次数的循环，同时也可以在循环内使用循环。for语句语法流程如下：

- init：一般为赋值表达式，给控制变量赋初值；
- condition：关系表达式或逻辑表达式，循环控制条件；
- post：一般为赋值表达式，修改赋值变量的值；

> init和post参数是可选的，可以直接省略它，类似于while语句。

![images](images/for_excute.png)

for循环有3种形式：

```Golang
/* 下面类似C语言for(init; condition; increment)语句 */
for init; condition; post {
}

/* 下面类似while语句 */
for condition {
}

/* 下面类似for(;;)语句 */
for {
}
```

for循环的range格式可以对slice、map、数组、字符串等进行迭代循环，格式如下：

```Golang
for key, value := range oldmap {
    newMap[key] = value
}
```

for语句也可以嵌套使用，或者执行无线循环，格式如下：

```Golang
/* for循环嵌套 */
for [condition | (init; condition; increment) | Range] {
    for [condition | (init; condition; increment) | Range] {
	    statement(s)
	}
	statement(s)
}

/* 无限循环 */
func main() {
	for true {
		fmt.Println("这是无限循环")
	}
}
```

2. 循环控制语句

循环控制语句可以控制循环体内语句的执行过程：
- break语句用于打断当前for循环或跳出switch语句，开始执行后面的语句；在多重循环中，可以使用标号label标出想break的循环（注意是break，不是goto）；
- continue语句用于跳过当前循环的剩余语句，然后继续执行下一轮循环，continue语句会触发for增量语句的执行；在多重循环中，可以使用标号label标出想continue的循环；
- goto语句可以无条件地将控制转移到被标记的语句，它常与条件语句配合，实现条件转移、构成循环或跳出循环等功能；在结构化程序设计中，不主张使用goto，以免造成程序流程混乱，加大理解和调试的困难。


```Golang
/* goto语法 */
    if condition
        goto label
... ...
label:
	statement(s)
```

## 九、Go语言函数

函数是基本的代码块，用于执行一个任务。你可以通过函数来划分不同的功能，逻辑上每个函数执行的是指定的任务。

### 9.1 函数定义

*函数声明*告诉了编译器函数的名称、返回值类型和参数类型。

Go语言函数定义格式如下：

```Golang
func function_name([parameter list]) [return_types list] {
	// 函数体
}
```

- ```func```：函数由```func```关键字开始声明；
- ```function_name```：函数名称，函数名和参数列表一起构成了函数签名；*与C语言一样，Go语言不支持函数重载（不同函数不能使用相同的函数名）*；
- ```parameter list```：参数列表，参数就像一个占位符，当函数被调用时，可以将值传递给参数，这个值被称为实际参数；参数列表指定了参数的类型、顺序以及参数个数；参数是可选的，也即函数可以不含参数；
- ```return_types```：返回类型，函数可以返回一列值；```return_types```是该列值的数据类型；返回类型是可选的，因为有些函数不需要返回值，我们也可以在函数体中显示的调用```return```语句来返回类型；
- 函数体：函数定义的代码集合，函数功能的代码实现。

### 9.2 函数返回

- 函数可以没有返回值；
- 函数的返回值可以只有一个，也可以有多个；
- 可以使用```return```语句显示执行返回或结束函数执行（可有返回值也可以没有）；
- 可以使用返回值类型中明确声明变量，```return```语句不带变量来实现单个或多个值的返回。

```Golang
/* 没有返回值的函数 */
func A() {
}

/* 有返回值的函数 */
func B() {
    var val int
    ... ...
    return val
}

/* 多个返回值的函数 */
func swap(a, b int){
    return b, a
}

/* 使用返回类型返回数据，如下将会顺序返回x, y */
func swap(a, b int) (x, y int) {
    x = b
    y = a
    return
}
```

### 9.3 函数调用

函数定义了指定的任务，我们通过函数调用来执行该任务。调用函数，向函数传递参数，执行完任务后返回值。Go语言在编译时，会执行函数参数类型匹配，如果类型不匹配，会报错（赋值语句一样会进行类型匹配）。

### 9.3 函数参数

调用函数时参数只有值传递方法：值传递是指在调用函数时将实际参数复制一份传递到函数中，这样在函数中如果对参数进行修改，将不会影响到实际参数；

我们可以使用地址传递来修改实参的值：地址传递是指在调用函数时将实际参数的地址传递到函数中，那么在函数中对参数所进行的修改，将影响到实际参数。

地址传递的使用方法与C语言类似：*函数定义时使用'\*'修饰，调用函数时使用‘&’修饰*。

```Golang
/* 值传递不修改实参的值 */
func swap(x, y int) {
    var temp int

    temp = x
    x = y
    y = temp
}

/* 地址传递可以修改实参的值 */
func swap(x *int, y* int) {
    var temp int

    temp = *x
    *x = *y
    *y = temp
}
```

### 9.4 函数高级用法

除了像C语言一样使用函数的方法外，Go语言还可以有如下几种函数使用方法。

- 匿名函数：函数定义不含名称，直接赋值给一个变量，可直接使用，也可以做为另外一个函数的实参传入

```Golang
package main
import (
    "fmt"
    "math"
)
func main() {
    /* 声明函数变量 */
    getSquareBoot := func(x float64) float64 {
        return math.Sqrt(x)
    }

    fmt.Println(getSquareBoot(9))
}
```

- 闭包：闭包是匿名函数，可在动态编译中使用。匿名函数是一个“内联”语句或表达式，匿名函数的优越性在于可以直接使用函数内的变量，不需要声明。

```Golang
package main
import "fmt"

func getSquence() func() int {
    i := 0
    return func() int {
        i += 1
        return i
    }
}

func main() {
    /* nextNumber是一个函数 */
    nextNumber := getSquence()
    fmt.Println(nextNumber())
    fmt.Println(nextNumber())
    fmt.Println(nextNumber())

    nextNumber1 := getSquence()
    fmt.Println(nextNumber1())
    fmt.Println(nextNumber1())
}
```

所谓闭包就是一个函数“捕获”了和它在同一作用域的其它常量和变量。这就意味着当闭包被调用的时候，不管在程序什么地方调用，闭包能够使用这些常量或者变量。它不关心这些捕获了的变量和常量是否已经超出了作用域，所以只有闭包还在使用它，这些变量就还会存在。

- 方法：方法就是一个包含了接受者的函数，接收者可以是命名类型或者结构体类型的一个值或者一个指针。所有给定类型的方法属于该类型的方法集。语法格式如下：

```Golang
func (variable_name variable_data_type) function_name() [return_type] {
    /* 函数体 */
}
```

下面定义了一个结构体类型和该类型的一个方法：

```Golang
package main
import "fmt"

type Circle struct {
    radius float64
}

func main() {
    var c1 Circle
    c1.radius = 10.00
    fmt.Println("圆的面积 = ", c1.getArea())
}

/*如下方法属于circle结构对象*/
func (c Circle) getArea() float64 {
    return 3.14 * c.radius * c.radius
}
```

### 9.5 递归函数

递归就是在运行的过程中调用自己。语法格式如下：

```Golang
func recursion() {
    recursion()
}

func main() {
    recursion()
}
```

在使用递归时，需要设置退出条件，否则递归将陷入无线循环中。

递归函数对于解决数学上的问题是非常有用的，就像计算阶乘，生成斐波那契数列等。

```Golang
package main

import "fmt"

func main() {
    var i int = 15
    fmt.Printf("%d的阶乘是:%d\n", i, Factorial(uint64(i)))

    fmt.Println("斐波那契数列:")
    for i = 0; i < 10; i++ {
        fmt.Printf("%d\t", fibonacci(i))
    }
    fmt.Printf("\n")
}

func Factorial(n uint64) uint64 {
    if(n > 0) {
        return (n * Factorial(n-1))
    }
    return 1
}

func fibonacci(n int) int {
    if n < 2 {
        return n
    }
    return fibonacci(n - 2) + fibonacci(n - 1)
}
```

## 十、Go语言错误处理

Go语言通过内置的错误接口，提供了非常简单的错误处理机制。error类型是一个接口类型，其定义如下：

```Golang
type error interface {
    Error() string
}
```

我们可以在编码中通过实现error接口类型来生成错误信息。比如，通常我们会在函数最后的返回值中返回错误信息（使用errors.New）：

```Golang
func Sqrt(f float64) (float64, error) {
    ... ...
    if f < 0 {
        return 0, errors.New("Math: square root of negative number")
    }
}
```

使用Sqrt()函数时，传入一个负数，就会得到non-nil的error对象：

```Golang
    result, err := Sqrt(-1)
    if(nil != err) {
        fmt.Println(err)
    }
```

## 十一、Go语言并发

```Go```语言中，使用```go```关键字来开启一个新的运行期线程```goroutine```。```goroutine```是轻量级线程，其调度由```Golang```运行时管理。同一个程序中的所有``goroutine```共享同一个地址空间。```goroutine```语法格式如下：

> go 函数名(参数列表)

```Golang
package main

import "fmt"
import "time"

func say(s string) {
    for i := 0; i < 5; i++ {
        time.Sleep(100 * time.Millisecond)
        fmt.Println(s)
    }
}

func main() {
    go say("world")
    say("hello")
}
```

### 11.1 通道类型（Channel）

通道（Channel）是传递数据的数据结构，用于两个```goroutine```之间传递一个指定类型的值来实现同步运行和通讯。操作符```<-```用于指定通道方向：发送或接收。若未指定方向，则为双向通道。

```Golang
ch <- v     /* 把v发送到通道ch */
v := <- ch  /* 从ch中接收数据，并赋值给v */
```

声明一个通道时使用关键字```chan```，通道在使用前必须创建：

> ch := make(chan int)

注意：默认情况下，通道是不带缓冲区的，发送端发送数据，同时必须有接收端处理接收数据，否则发送端会阻塞。

可以通过```make```的第二个参数指定缓冲区大小：

> ch := make(chan int, 100)

带缓冲区的通道允许发送端的数据发送和接收端的数据获取处于异步状态，也就是发送端发送端的数据可以放在缓冲区里面。不过由于缓冲区的大小是有限的（内存是受限的），还是需要接收端去接收和处理数据，否则缓冲区一满，数据发送端就无法再发送数据了。

注意：如果通道不带缓冲，发送方会阻塞直到接收方从通道中接收了数据。如果通道带缓冲区，发送方则会阻塞直到发送的值被拷贝到缓冲区中；如果缓冲区已满，则意味着需要等待直到某个接收方获取到一个值。接收方则是在有数据值可接收之前一直阻塞。

我们可以通过```range```关键字来遍历读取到的数据，类似于数组或切片，格式如下：

> v, ok := <-ch

如果通道接收不到数据，ok就为```false```，这时通道就可以用```close()```函数来关闭。
