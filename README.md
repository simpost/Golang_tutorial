# ```Golang```学习笔记

## 一、Go环境与工具

- Sublime编辑工具的安装与使用：[Sublime](./Go环境与工具/sublime.md)

## 二、Go语言教程

教程链接: https://www.runoob.com/go/go-tutorial.html

学习笔记：[《Go语言教程》笔记](./Go语言教程/Golang.md)

建议学习时长：一周(7天内)

## 三、Go编程基础（进行中）

视频链接：https://study.163.com/course/courseLearn.htm?courseId=306002#/learn/video?lessonId=421012&courseId=306002

课堂笔记：https://github.com/Unknwon/go-fundamental-programming

建议学习时长：一周（7天内）

## 四、Go入门指南（进行中）

书籍链接：http://books.studygolang.com/the-way-to-go_ZH_CN/

学习笔记：[《Go入门指南》笔记](./Go入门指南/Golang.md)

建议学习时长：一周（7天内）

## 五、Go语言圣经（未开始）

书籍链接：http://books.studygolang.com/gopl-zh/

学习笔记：[《Go语言圣经》笔记](./Go语言圣经/Golang.md)

建议学习时长：两周（14天内）

## 六、Go高级编程（未开始）

书籍链接：https://books.studygolang.com/advanced-go-programming-book/

学习笔记：[《Go高级编程》笔记](./Go高级编程/Golang.md)

建议学习时长：两周（14天内）

## 七、Go设计模式


## 八、深入解析Go


## 九、其他资料

- [Go语言官网](https://golang.google.cn/dl/)
- [Go语言中文网](https://studygolang.com/)
- [Go语言网络编程](http://books.studygolang.com/NPWG_zh/)
- [Go语言标准库](http://books.studygolang.com/The-Golang-Standard-Library-by-Example/)
- [Go RPC开发指南](http://books.studygolang.com/go-rpc-programming-guide/)
- [Go语言语法树入门](https://github.com/chai2010/go-ast-book)


## 九、项目实战

### 9.1 Go构建Web服务器


### 9.2 Go高并发后台服务


### 9.3 Go后台微服务


## Go语言的问题

学习过程中，遇见的经典问题，总结在这里：[Go问题笔记](./Go问题笔记/GoProblems.md)

## 待完成的学习内容

- Go语言不通环境下的搭建手册，独立于语言介绍的文档，可连接到官网下载与安装使用说明；
- Go语言与C语言的差异
- Go与C联合编程
- 专门的文档介绍高并发实现；
- 专门的文档介绍WEB服务器搭建；
- 专门的文档介绍.IO操作、字节操作、堆列表环形队列等容器、数据库操作、调试方法、JSON/XML等、格式化操作、数学库、网络库、反射机制、时间、文本等等方法实际演练；
- Go语言圣经学习
